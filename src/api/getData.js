export default async function getData(){

    async function fetchData() {
        let response = await fetch('https://api.spacexdata.com/v3/launches')
        let data = await response.json()
        console.log(data)
        return await data.reverse()
      }

      let Result = await fetchData();

      return await Result
}

export async function getRockets(){

    async function fetchRockets() {
      let response = await fetch('https://api.spacexdata.com/v3/rockets')
      let data = await response.json()
      console.log(data)
      return data
    }

    let Rockets = await fetchRockets();

    return await Rockets
}