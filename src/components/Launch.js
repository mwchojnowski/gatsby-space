import React from "react"

export default function Launch({
    rocket_name,
    launch_site,
    launch_date,
    mission_name,
    flight_number,
    details,
    url,
    rockets,
    rocket_id
}){
    let link ="https://www.youtube.com/embed/"+url
    return(
        <div className="launch sm:w-3/4 base:w-full flex xl:flex-row base:flex-col p-15 bg-gray-500 mb-10 rounded-2xl">
            <div className="w-full p-10 flex align-center justify-center">
                  <iframe src={link} allowFullScreen className="w-full">
                      Cannot play Video
                  </iframe>
            </div>
            <div className="w-full p-10">
                <h1 className="text-white font-bold">{rocket_name}</h1>
                {rockets.map(rocket=>(
                    (rocket.rocket_id === rocket_id) &&
                    <>
                        <h1 className="text-white">Cost per Launch: ${rocket.cost_per_launch}</h1>
                        <h1 className="text-white">Weight(lbs): {rocket.mass.lb}</h1>
                        <br/>
                    </>
                ))}
                <h1 className="text-white font-semibold">Launch site: {launch_site}</h1>
                <h1 className="text-white">Launched: {launch_date}</h1>
                <h1 className="text-white">Mission name: {mission_name}</h1>
                <h1 className="text-white">Flight number: {flight_number}</h1>
                <br/>
                <h1 className="text-white ">Details:</h1>
                <h1 className="text-white">
                    {details}
                    {details===null || details === '' || details ===undefined && 
                        <h1>
                            No description provided
                        </h1>
                    }
                </h1>
            </div>
        </div>
    )
}