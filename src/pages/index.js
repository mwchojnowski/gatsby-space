import React from 'react'
import LaunchDisplay from "../components/LaunchDisplay"

const IndexPage = () => {
	return (
		<div className="w-full flex justify-center bg-gray-800 pt-20">
			<LaunchDisplay />
		</div>
	)
}

export default IndexPage
